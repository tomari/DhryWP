﻿//
// MainPage.xaml.h
// Declaration of the MainPage class.
//

#pragma once

#include "MainPage.g.h"
#include "DhryContext.h"

namespace DhryWP
{
	/// <summary>
	/// An empty page that can be used on its own or navigated to within a Frame.
	/// </summary>
	public ref class MainPage sealed: public DhryPageInterface
	{
	public:
		MainPage();
		virtual void updatePageBeforeBenchmark();
		virtual void updatePageAfterBenchmark();

	protected:
		virtual void OnNavigatedTo(Windows::UI::Xaml::Navigation::NavigationEventArgs^ e) override;
	private:
		DhryContext^ dhrycontext;
	};
}
