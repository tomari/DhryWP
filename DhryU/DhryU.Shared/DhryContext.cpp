﻿#include "pch.h"
#include "DhryContext.h"
#include "DelegateCommand.h"
#include "dhryglue.h"

using namespace DhryWP;
using namespace Platform;
using namespace Windows::UI::Xaml::Data;
using namespace concurrency;

#define DHRYCONTEXT_TOSTR2(y) L#y
#define DHRYCONTEXT_TOSTR(x) DHRYCONTEXT_TOSTR2(x)

DhryContext::DhryContext(DhryPageInterface^ _page) :
	page(_page),
	dhryrunning(false)
{
	NLoopsText = ref new String();

	std::wstringstream converter;
	wchar_t szptr[16];
	converter << (8 * sizeof(void*));
	converter >> szptr;
	LogBuf = ref new String(L"Compiled with "
#ifdef _MSC_VER
		L"_MSC_FULL_VER=" DHRYCONTEXT_TOSTR(_MSC_FULL_VER) L" _MSC_BUILD=" DHRYCONTEXT_TOSTR(_MSC_BUILD)
#else
		L"unknown compiler"
#endif
		L"\nsize of pointers = ")+
		ref new Platform::String(szptr)+
		L" bits";
	
	runcommand = ref new DelegateCommand(
		ref new ExecuteDelegate(this, &DhryContext::RunDhry),
		ref new CanExecuteDelegate(this, &DhryContext::CanRunDhry));
}

void
DhryContext::RunDhry(Platform::Object^ parameter) {
	(void)parameter;
	dhryrunning = true;
	page->updatePageBeforeBenchmark();
	auto dhryaction = create_async([&]() {
		std::wstringstream converter;
		std::wstring nloopsstr((NLoopsText->Data()));
		converter << nloopsstr;
		long nloops_long;
		converter >> nloops_long;
		Platform::String^ resstr;
		if (nloops_long > 0L) {
			std::this_thread::sleep_for(std::chrono::milliseconds(700)); // Wait for UI elements to settle
			wchar_t *res_char = run_dhry(nloops_long);
			resstr = ref new String(res_char);
		} else {
			resstr = L"Specify positive number of runs.\r\n"
				L"Try 1000000.\r\n";
		}
		return LogBuf + resstr;
	});
	auto dhrytask = create_task(dhryaction);
	dhrytask.then([&](String^ res) {
		LogBuf = res;
		OnPropertyChanged(L"LogBuf");
		dhryrunning = false;
		page->updatePageAfterBenchmark();
	});
}

