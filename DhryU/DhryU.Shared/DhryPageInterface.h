namespace DhryWP {
	public interface class DhryPageInterface {
	public:
		virtual void updatePageBeforeBenchmark() = 0;
		virtual void updatePageAfterBenchmark() = 0;
	};
}
