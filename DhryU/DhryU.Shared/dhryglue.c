#include <stdlib.h>
#include "dhryglue.h"

wchar_t outbuf[OUTBUF_SZ];
wchar_t *obufp;

extern wchar_t *
run_dhry(long nloops) {
	outbuf[0] = L'\0';
	obufp = &outbuf[0];
	//obufp += swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"%ld\n", nloops);
	DhryMain(nloops);
	return outbuf;
}
