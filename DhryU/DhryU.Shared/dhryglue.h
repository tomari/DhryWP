#include <stdio.h>
#include <wchar.h>

#ifdef __cplusplus
extern "C" {
#endif

	#define OUTBUF_SZ 16384
	extern wchar_t outbuf[], *obufp;

	extern wchar_t *run_dhry(long nloops);
	extern int DhryMain(long nloops);

#ifdef __cplusplus
}
#endif
