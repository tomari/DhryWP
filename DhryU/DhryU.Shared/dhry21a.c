/*
 *************************************************************************
 *
 *                   "DHRYSTONE" Benchmark Program
 *                   -----------------------------
 *
 *  Version:    C, Version 2.1
 *
 *  File:       dhry_1.c (part 2 of 3)
 *
 *  Date:       May 25, 1988
 *
 *  Author:     Reinhold P. Weicker
 *
 *************************************************************************
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "dhryglue.h"
#include "dhry.h"

#include <stdarg.h>
static intptr_t dhry_min(intptr_t x, intptr_t y) { return x<y?x:y; }
static intptr_t dhry_swprintf(wchar_t * str, intptr_t size, const wchar_t * format, ...) {
	va_list ap;
	if(size>0) {
		va_start(ap, format);
		int r=vswprintf(str,size,format,ap);
		va_end(ap);
		return dhry_min(size,r);
	} else {
		return 0;
	}
}

/* Global Variables: */

Rec_Pointer     Ptr_Glob,
                Next_Ptr_Glob;
int             Int_Glob;
Boolean         Bool_Glob;
char            Ch_1_Glob,
                Ch_2_Glob;
int             Arr_1_Glob [50];
int             Arr_2_Glob [50] [50];

char Reg_Define[] = "Register option selected.";

/*extern char     *malloc ();*/
Enumeration     Func_1 ();
  /* forward declaration necessary since Enumeration may not simply be int */


#ifndef ROPT
#define REG
        /* REG becomes defined as empty */
        /* i.e. no register variables   */
#else
#define REG register
#endif

void Proc_1(REG Rec_Pointer);
void Proc_2 (One_Fifty *);
void Proc_3 (Rec_Pointer *);
void Proc_4 (void);
void Proc_5 (void);
#ifdef  NOSTRUCTASSIGN
 void memcpy (register char *, register char *, register int);
#endif

extern Boolean Func_2 (Str_30, Str_30);
extern void Proc_6 (Enumeration, Enumeration *);
extern void Proc_7 (One_Fifty, One_Fifty, One_Fifty *);
extern void Proc_8 (Arr_1_Dim, Arr_2_Dim, int, int);


/* variables for time measurement: */

#define Too_Small_Time 2
                /* Measurements should last at least 2 seconds */

double          Begin_Time,
                End_Time,
                User_Time;

double          Microseconds,
                Dhrystones_Per_Second,
                Vax_Mips;

/* end of variables for time measurement */


int
DhryMain (long nloops)
/*****/

  /* main program, corresponds to procedures        */
  /* Main and Proc_0 in the Ada version             */
{
  double   dtime();

        One_Fifty       Int_1_Loc = 0;
  REG   One_Fifty       Int_2_Loc = 0;
        One_Fifty       Int_3_Loc;
  REG   char            Ch_Index;
        Enumeration     Enum_Loc;
        Str_30          Str_1_Loc;
        Str_30          Str_2_Loc;
  REG   long            Run_Index;
  REG   long            Number_Of_Runs;

        FILE            *Ap;

  /* Initializations */
#if 0
 #ifdef riscos
  if ((Ap = fopen("dhry/res","a+")) == NULL)
 #else
  if ((Ap = fopen("dhry.res","a+")) == NULL)
 #endif
    {
       printf("Can not open dhry.res\n\n");
       exit(1);
    }
#endif

  Next_Ptr_Glob = (Rec_Pointer) malloc (sizeof (Rec_Type));
  Ptr_Glob = (Rec_Pointer) malloc (sizeof (Rec_Type));

  Ptr_Glob->Ptr_Comp                    = Next_Ptr_Glob;
  Ptr_Glob->Discr                       = Ident_1;
  Ptr_Glob->variant.var_1.Enum_Comp     = Ident_3;
  Ptr_Glob->variant.var_1.Int_Comp      = 40;
  strcpy (Ptr_Glob->variant.var_1.Str_Comp,
          "DHRYSTONE PROGRAM, SOME STRING");
  strcpy (Str_1_Loc, "DHRYSTONE PROGRAM, 1'ST STRING");

  Arr_2_Glob [8][7] = 10;
        /* Was missing in published program. Without this statement,    */
        /* Arr_2_Glob [8][7] would have an undefined value.             */
        /* Warning: With 16-Bit processors and Number_Of_Runs > 32000,  */
        /* overflow may occur for this array element.                   */

  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"\r\n");
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"Dhrystone Benchmark, Version 2.1 (Language: C)\r\n");
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"\r\n");
/*
  if (Reg)
  {
    printf ("Program compiled with 'register' attribute\n");
    printf ("\n");
  }
  else
  {
    printf ("Program compiled without 'register' attribute\n");
    printf ("\n");
  }
*/
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"Please give the number of runs through the benchmark: ");
  {
#if 0
    long n;
    scanf ("%ld", &n);
#endif
    Number_Of_Runs = nloops;
  }
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"\r\n");

  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf,L"Execution starts, %d runs through Dhrystone\r\n",Number_Of_Runs);

  /***************/
  /* Start timer */
  /***************/

  Begin_Time = dtime();

  for (Run_Index = 1; Run_Index <= Number_Of_Runs; ++Run_Index)
  {

    Proc_5();
    Proc_4();
      /* Ch_1_Glob == 'A', Ch_2_Glob == 'B', Bool_Glob == true */
    Int_1_Loc = 2;
    Int_2_Loc = 3;
    strcpy (Str_2_Loc, "DHRYSTONE PROGRAM, 2'ND STRING");
    Enum_Loc = Ident_2;
    Bool_Glob = ! Func_2 (Str_1_Loc, Str_2_Loc);
      /* Bool_Glob == 1 */
    while (Int_1_Loc < Int_2_Loc)  /* loop body executed once */
    {
      Int_3_Loc = 5 * Int_1_Loc - Int_2_Loc;
        /* Int_3_Loc == 7 */
      Proc_7 (Int_1_Loc, Int_2_Loc, &Int_3_Loc);
        /* Int_3_Loc == 7 */
      Int_1_Loc += 1;
    } /* while */
      /* Int_1_Loc == 3, Int_2_Loc == 3, Int_3_Loc == 7 */
    Proc_8 (Arr_1_Glob, Arr_2_Glob, Int_1_Loc, Int_3_Loc);
      /* Int_Glob == 5 */
    Proc_1 (Ptr_Glob);
    for (Ch_Index = 'A'; Ch_Index <= Ch_2_Glob; ++Ch_Index)
                             /* loop body executed twice */
    {
      if (Enum_Loc == Func_1 (Ch_Index, 'C'))
          /* then, not executed */
        {
        Proc_6 (Ident_1, &Enum_Loc);
        strcpy (Str_2_Loc, "DHRYSTONE PROGRAM, 3'RD STRING");
        Int_2_Loc = Run_Index;
        Int_Glob = Run_Index;
        }
    }
      /* Int_1_Loc == 3, Int_2_Loc == 3, Int_3_Loc == 7 */
    Int_2_Loc = Int_2_Loc * Int_1_Loc;
    Int_1_Loc = Int_2_Loc / Int_3_Loc;
    Int_2_Loc = 7 * (Int_2_Loc - Int_3_Loc) - Int_1_Loc;
      /* Int_1_Loc == 1, Int_2_Loc == 13, Int_3_Loc == 7 */
    Proc_2 (&Int_1_Loc);
      /* Int_1_Loc == 5 */

  } /* loop "for Run_Index" */

  /**************/
  /* Stop timer */
  /**************/

  End_Time = dtime();

  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"Execution ends\r\n");
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"\r\n");
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"Final values of the variables used in the benchmark:\r\n");
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"\r\n");
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"Int_Glob:            %d\r\n", Int_Glob);
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"        should be:   %d\r\n", 5);
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"Bool_Glob:           %d\r\n", Bool_Glob);
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"        should be:   %d\r\n", 1);
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"Ch_1_Glob:           %c\r\n", Ch_1_Glob);
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"        should be:   %c\r\n", 'A');
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"Ch_2_Glob:           %c\r\n", Ch_2_Glob);
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"        should be:   %c\r\n", 'B');
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"Arr_1_Glob[8]:       %d\r\n", Arr_1_Glob[8]);
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"        should be:   %d\r\n", 7);
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"Arr_2_Glob[8][7]:    %d\r\n", Arr_2_Glob[8][7]);
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"        should be:   Number_Of_Runs + 10\r\n");
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"Ptr_Glob->\r\n");
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"  Ptr_Comp:          %ld\r\n", (long) Ptr_Glob->Ptr_Comp);
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"        should be:   (implementation-dependent)\r\n");
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"  Discr:             %d\r\n", Ptr_Glob->Discr);
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"        should be:   %d\r\n", 0);
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"  Enum_Comp:         %d\r\n", Ptr_Glob->variant.var_1.Enum_Comp);
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"        should be:   %d\r\n", 2);
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"  Int_Comp:          %d\r\n", Ptr_Glob->variant.var_1.Int_Comp);
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"        should be:   %d\r\n", 17);
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"  Str_Comp:          %S\r\n", Ptr_Glob->variant.var_1.Str_Comp);
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"        should be:   DHRYSTONE PROGRAM, SOME STRING\r\n");
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"Next_Ptr_Glob->\r\n");
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"  Ptr_Comp:          %ld\r\n", (long) Next_Ptr_Glob->Ptr_Comp);
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"        should be:   (implementation-dependent), same as above\r\n");
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"  Discr:             %d\r\n", Next_Ptr_Glob->Discr);
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"        should be:   %d\r\n", 0);
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"  Enum_Comp:         %d\r\n", Next_Ptr_Glob->variant.var_1.Enum_Comp);
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"        should be:   %d\r\n", 1);
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"  Int_Comp:          %d\r\n", Next_Ptr_Glob->variant.var_1.Int_Comp);
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"        should be:   %d\r\n", 18);
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"  Str_Comp:          %S\r\n", Next_Ptr_Glob->variant.var_1.Str_Comp);
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"        should be:   DHRYSTONE PROGRAM, SOME STRING\r\n");
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"Int_1_Loc:           %d\r\n", Int_1_Loc);
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"        should be:   %d\r\n", 5);
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"Int_2_Loc:           %d\r\n", Int_2_Loc);
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"        should be:   %d\r\n", 13);
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"Int_3_Loc:           %d\r\n", Int_3_Loc);
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"        should be:   %d\r\n", 7);
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"Enum_Loc:            %d\r\n", Enum_Loc);
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"        should be:   %d\r\n", 1);
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"Str_1_Loc:           %S\r\n", Str_1_Loc);
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"        should be:   DHRYSTONE PROGRAM, 1'ST STRING\r\n");
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"Str_2_Loc:           %S\r\n", Str_2_Loc);
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"        should be:   DHRYSTONE PROGRAM, 2'ND STRING\r\n");
  obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"\r\n");

  User_Time = End_Time - Begin_Time;

  if (User_Time < Too_Small_Time)
  {
    obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"Measured time insufficient to obtain meaningful results:");
    obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"%10.1f seconds\r\n",User_Time);
    obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"Please increase number of runs so that benchmark runs for over 2.0 seconds\r\n");
    obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"\r\n");
  }
  else
  {
    Microseconds = User_Time * Mic_secs_Per_Second
                        / (double) Number_Of_Runs;
    Dhrystones_Per_Second = (double) Number_Of_Runs / User_Time;
    Vax_Mips = Dhrystones_Per_Second / 1757.0;

#ifdef ROPT
    obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"Register option selected?  YES\r\n");
#else
    obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"Register option selected?  NO\r\n");
    strcpy(Reg_Define, "Register option not selected.");
#endif
    obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"Seconds for run through Dhrystone loops:    ");
    obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"%10.1f \r\n", User_Time);
    obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"Microseconds for one run through Dhrystone: ");
    obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"%7.1f \r\n", Microseconds);
    obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"Dhrystones per Second:                      ");
    obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"%10.1f \r\n", Dhrystones_Per_Second);
    obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"VAX MIPS rating = %10.3f \r\n",Vax_Mips);
    obufp += dhry_swprintf(obufp, &obufp[OUTBUF_SZ]-outbuf, L"\r\n");

#if 0
  fprintf(Ap,"\r\n");
  fprintf(Ap,"Dhrystone Benchmark, Version 2.1 (Language: C)\r\n");
  fprintf(Ap,"%s\r\n",Reg_Define);
  fprintf(Ap,"Seconds for run through Dhrystone loops: ");
  fprintf(Ap,"%10.1f \r\n", User_Time);
  fprintf(Ap,"Microseconds for one loop: %7.1f\r\n",Microseconds);
  fprintf(Ap,"Dhrystones per second: %10.1f\r\n",Dhrystones_Per_Second);
  fprintf(Ap,"VAX MIPS rating: %10.3f\r\n",Vax_Mips);
  fclose(Ap);
#endif

  }
	free(Ptr_Glob);
	free(Next_Ptr_Glob);
	return 0;
}


void
Proc_1 (REG Rec_Pointer Ptr_Val_Par)
    /* executed once */
{
  REG Rec_Pointer Next_Record = Ptr_Val_Par->Ptr_Comp;
                                        /* == Ptr_Glob_Next */
  /* Local variable, initialized with Ptr_Val_Par->Ptr_Comp,    */
  /* corresponds to "rename" in Ada, "with" in Pascal           */

  structassign (*Ptr_Val_Par->Ptr_Comp, *Ptr_Glob);
  Ptr_Val_Par->variant.var_1.Int_Comp = 5;
  Next_Record->variant.var_1.Int_Comp
        = Ptr_Val_Par->variant.var_1.Int_Comp;
  Next_Record->Ptr_Comp = Ptr_Val_Par->Ptr_Comp;
  Proc_3 (&Next_Record->Ptr_Comp);
    /* Ptr_Val_Par->Ptr_Comp->Ptr_Comp
                        == Ptr_Glob->Ptr_Comp */
  if (Next_Record->Discr == Ident_1)
    /* then, executed */
  {
    Next_Record->variant.var_1.Int_Comp = 6;
    Proc_6 (Ptr_Val_Par->variant.var_1.Enum_Comp,
           &Next_Record->variant.var_1.Enum_Comp);
    Next_Record->Ptr_Comp = Ptr_Glob->Ptr_Comp;
    Proc_7 (Next_Record->variant.var_1.Int_Comp, 10,
           &Next_Record->variant.var_1.Int_Comp);
  }
  else /* not executed */
    structassign (*Ptr_Val_Par, *Ptr_Val_Par->Ptr_Comp);
} /* Proc_1 */


void
Proc_2 (One_Fifty *Int_Par_Ref)
/******************/
    /* executed once */
    /* *Int_Par_Ref == 1, becomes 4 */
{
  One_Fifty  Int_Loc;
  Enumeration   Enum_Loc = Ident_1;

  Int_Loc = *Int_Par_Ref + 10;
  do /* executed once */
    if (Ch_1_Glob == 'A')
      /* then, executed */
    {
      Int_Loc -= 1;
      *Int_Par_Ref = Int_Loc - Int_Glob;
      Enum_Loc = Ident_1;
    } /* if */
  while (Enum_Loc != Ident_1); /* true */
} /* Proc_2 */


void
Proc_3 (Rec_Pointer *Ptr_Ref_Par)
/******************/
    /* executed once */
    /* Ptr_Ref_Par becomes Ptr_Glob */
{
  if (Ptr_Glob != Null)
    /* then, executed */
    *Ptr_Ref_Par = Ptr_Glob->Ptr_Comp;
  Proc_7 (10, Int_Glob, &Ptr_Glob->variant.var_1.Int_Comp);
} /* Proc_3 */


void
Proc_4 (void) /* without parameters */
/*******/
    /* executed once */
{
  Boolean Bool_Loc;

  Bool_Loc = Ch_1_Glob == 'A';
  Bool_Glob = Bool_Loc | Bool_Glob;
  Ch_2_Glob = 'B';
} /* Proc_4 */


void
Proc_5 (void) 	/* without parameters */
		/* executed once */
{
  Ch_1_Glob = 'A';
  Bool_Glob = false;
} /* Proc_5 */


        /* Procedure for the assignment of structures,          */
        /* if the C compiler doesn't support this feature       */
#ifdef  NOSTRUCTASSIGN
void
memcpy (register char *d, register char *s, register int l)
/* register char   *d; register char   *s; register int    l; */
{
        while (l--) *d++ = *s++;
}
#endif
