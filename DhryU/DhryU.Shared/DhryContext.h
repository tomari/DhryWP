﻿#pragma once
#include "pch.h"
#include "BindableBase.h"
#include "DhryPageInterface.h"

namespace DhryWP {
	//
    [Windows::UI::Xaml::Data::Bindable]
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class DhryContext sealed : Common::BindableBase {
	public:
		DhryContext(DhryPageInterface^ _page);
		property Platform::String^ NLoopsText;
		property Platform::String^ LogBuf;
		property Windows::UI::Xaml::Input::ICommand^ RunDhryCommand
		{
			Windows::UI::Xaml::Input::ICommand^ get() { return runcommand; }
		}
	private:
		Windows::UI::Xaml::Input::ICommand^ runcommand;
		void RunDhry(Platform::Object^ parameter);
		bool CanRunDhry(Platform::Object^ parameter) { (void)parameter; return !dhryrunning; }
		bool dhryrunning;
		DhryPageInterface^ page;
	};

}

